#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    painter()
{
    ui->setupUi(this);

    this->setFixedSize(QSize(438,550));
    setAttribute(Qt::WA_StaticContents);

    //Set background
    QPixmap bkgnd("/Users/pasutapaopun/Desktop/OOPPro1/Resource/bg.jpg");
    QPalette palette;
    palette.setBrush(QPalette::Background, bkgnd);
    this->setPalette(palette);


    //Resource
    cur_pix = QPixmap("/Users/pasutapaopun/Desktop/OOPPro1/Resource/Cur_score.png");
    high_score_pix = QPixmap("/Users/pasutapaopun/Desktop/OOPPro1/Resource/Highscore.png");
    button_pix = QPixmap("/Users/pasutapaopun/Desktop/OOPPro1/Resource/giveUp.png");
    retry_pix = QPixmap("/Users/pasutapaopun/Desktop/OOPPro1/Resource/retry.png");


    //Set button
    ui->giveupp->setIcon(QIcon(button_pix));
    ui->giveupp->setIconSize(QSize(180,30));

    ui->retryy->setIcon(QIcon(retry_pix));
    ui->retryy->setIconSize(QSize(180,30));



    //Array for pic resource
        //Block
    block_pixmaps[0] = QPixmap("/Users/pasutapaopun/Desktop/OOPPro1/Resource/Block1.png");
    block_pixmaps[1] = QPixmap("/Users/pasutapaopun/Desktop/OOPPro1/Resource/SBlock.png");
    block_pixmaps[2] = QPixmap("/Users/pasutapaopun/Desktop/OOPPro1/Resource/NreBlock.png");
    block_pixmaps[3] = QPixmap("/Users/pasutapaopun/Desktop/OOPPro1/Resource/NBlockk.png");
    block_pixmaps[4] = QPixmap("/Users/pasutapaopun/Desktop/OOPPro1/Resource/LreBlock.png");
    block_pixmaps[5] = QPixmap("/Users/pasutapaopun/Desktop/OOPPro1/Resource/Iblockk.png");

       //Number for score
    num_pixmap[0] = QPixmap("/Users/pasutapaopun/Desktop/OOPPro1/Resource/zero.png");
    num_pixmap[1] = QPixmap("/Users/pasutapaopun/Desktop/OOPPro1/Resource/one.png");
    num_pixmap[2] = QPixmap("/Users/pasutapaopun/Desktop/OOPPro1/Resource/two.png");
    num_pixmap[3] = QPixmap("/Users/pasutapaopun/Desktop/OOPPro1/Resource/three.png");
    num_pixmap[4] = QPixmap("/Users/pasutapaopun/Desktop/OOPPro1/Resource/four.png");
    num_pixmap[5] = QPixmap("/Users/pasutapaopun/Desktop/OOPPro1/Resource/five.png");
    num_pixmap[6] = QPixmap("/Users/pasutapaopun/Desktop/OOPPro1/Resource/six.png");
    num_pixmap[7] = QPixmap("/Users/pasutapaopun/Desktop/OOPPro1/Resource/seven.png");
    num_pixmap[8] = QPixmap("/Users/pasutapaopun/Desktop/OOPPro1/Resource/eight.png");
    num_pixmap[9] = QPixmap("/Users/pasutapaopun/Desktop/OOPPro1/Resource/nine.png");

    //Initialized coordinate
    start_x = ui->board->x();
    start_y = ui->board->y();

    //Array 2D for the board
    memset(full, 0, sizeof(full[0][0] * BOARDSIZE * BOARDSIZE));
    for (int i = 0; i < BOARDSIZE; i++) {
        for (int j = 0; j < BOARDSIZE; j++) {
            full[i][j]=0;
        }
    }


    //Random block
    current = getRandomPiece();
    next = getRandomPiece();


    ui->board->setVisible(false);

    writeHighscore();

    setWindowTitle("Draw-the-Tetra");



}

MainWindow::~MainWindow()
{
    delete ui;
}


//Mouse event for press and release
void MainWindow::mouseMoveEvent(QMouseEvent *mouse_event) {
    int x = mouse_event->x() - start_x;
    int y = mouse_event->y() - start_y;
    if (x >= 0 && x <= 399 && y >= 0 && y <= 399) {
        int grid_x = x / 50;
        int grid_y = y / 50;
        if (full[grid_y][grid_x] != 0) {
            drawn_blocks.clear();
            return;
        }
        drawn_blocks.insert(std::pair<int, int>(grid_x, grid_y));
        if (drawn_blocks.size() == 4) {
            nextBlock();
        }
    }

}


void MainWindow::nextBlock() {
    if (current->check(drawn_blocks)) {
        std::set<std::pair<int, int>>::iterator it;
        for  (it = drawn_blocks.begin(); it != drawn_blocks.end(); it++) {
            full[it->second][it->first] = current->getNum();
        }

        //random new block
        Piece *temp = current;
        current = next;
        next = getRandomPiece();
        checkComplete();
        delete temp;

    }
    drawn_blocks.clear();
}


//Check if the line is completed and clear
void MainWindow::checkComplete() {
    std::vector<int> to_delete_row, to_delete_col;
    for (int i = 0; i < BOARDSIZE; i++) {
        for (int j = 0; j < BOARDSIZE; j++) {
            if (full[i][j] == 0) {
                break;
            }
            if (j == 7) {
                to_delete_row.push_back(i);

            }
        }
    }
    for (int i = 0; i < BOARDSIZE; i++) {
        for (int j = 0; j < BOARDSIZE; j++) {
            if (full[j][i] == 0) {
                break;
            }
            if (j == 7) {
                to_delete_col.push_back(i);

            }
        }
    }

    for(int i = 0; i < to_delete_col.size(); i++) {
        for(int j = 0; j < BOARDSIZE; j++) {
            full[j][to_delete_col[i]] = 0;

        }
    }
    for(int i = 0; i < to_delete_row.size(); i++) {
        for(int j = 0; j < BOARDSIZE; j++) {
            full[to_delete_row[i]][j] = 0;

        }
    }

    //score
    cur_score += ((to_delete_row.size()*20) /7) *10;
    cur_score += ((to_delete_col.size()*20) /7) * 10;
    writeHighscore();

}

//Draw the block to the board
void MainWindow::paintEvent(QPaintEvent *paint_event) {
    QPainter painter(this);
    for (int i = 0; i < BOARDSIZE; i++) {
        for (int j = 0; j < BOARDSIZE; j++) {
            if (full[i][j]) {
                painter.drawPixmap(QRect(QPoint(j * 50 + start_x, i * 50 + start_y),
                                   QSize(50, 50)), block_pixmaps[full[i][j] - 1]);
            }
        }
    }

    if (current != NULL)
    painter.drawPixmap(QRect(QPoint(180, 40),
                             QSize(90, 90)), current->getPic());
    painter.drawPixmap(QRect(QPoint(325, 5),
                             QSize(60, 60)), next->getPic());
    painter.drawPixmap(QRect(QPoint(10,10),
                             QSize(146,40)),
                       QPixmap("/Users/pasutapaopun/Desktop/OOPPro1/Resource/Cur_score.png"));
    painter.drawPixmap(QRect(QPoint(10, 65),
                             QSize(112, 40)), high_score_pix);

    painter.end();
    this->update();
}

void MainWindow::mousePressEvent(QMouseEvent *mouse_event) {
    drawn_blocks.clear();
    pressed = true;
}

void MainWindow::endMouseEvent() {
    std::set<std::pair<int, int>>::iterator it;
    for  (it = drawn_blocks.begin(); it != drawn_blocks.end(); it++) {
        std::cout << (*it).first << ", " << (*it).second << " ";
    }
    pressed = false;
}

//Random 6 pieces
Piece* MainWindow::getRandomPiece() {
    int piece = QRandomGenerator::global()->bounded(6)+1;
    switch ( piece ) {
    case 1:
      return new LBlock();
    case 2:
      return new SquareBlock();
    case 3:
      return new NreBlock();
    case 4:
      return new NBlock();
    case 5:
      return new LreBlock();
    case 6:
      return new IBlock();
    }
}

//Check & write highscore and display the score
void MainWindow::writeHighscore(){
    std::ifstream readFile;
    readFile.open("/Users/pasutapaopun/Desktop/OOPPro1/HIgh_score.txt");

    if(readFile.is_open()){

        while(!readFile.eof()){
            readFile >> highscore;
        }

    }

    readFile.close();

    std::ofstream writeFile("/Users/pasutapaopun/Desktop/OOPPro1/HIgh_score.txt");

    if(writeFile.is_open()){
        if( cur_score > highscore ){
            highscore = cur_score;
        }
        writeFile << highscore;
    }

    writeFile.close();

    int d1,d2,d3,d4,d5,d6;
    d6 = cur_score % 10;
    d5 = (cur_score / 10) % 10;
    d4 = (cur_score / 100) % 10;
    d3 = (cur_score / 1000) % 10;
    d2 = (cur_score / 10000) % 10;
    d1 = (cur_score / 100000) % 10;

    int h1,h2,h3,h4,h5,h6;
    h6 = highscore % 10;
    h5 = (highscore / 10) % 10;
    h4 = (highscore / 100) % 10;
    h3 = (highscore / 1000) % 10;
    h2 = (highscore / 10000) % 10;
    h1 = (highscore / 100000) % 10;

    if(cur_score >= 0){
         ui->one_cur->setPixmap(num_pixmap[d6]);
         if(cur_score >= 10){
             ui->sib_cur->setPixmap(num_pixmap[d5]);
             if(cur_score >= 100){
                 ui->roi_cur->setPixmap(num_pixmap[d4]);
                 if(cur_score >= 1000){
                     ui->pan_cur->setPixmap(num_pixmap[d3]);
                     if(cur_score >= 10000){
                         ui->mh_cur->setPixmap(num_pixmap[d2]);
                         if(cur_score >= 100000){
                              ui->san_cur->setPixmap(num_pixmap[d1]);
                         }
                     }
                 }
             }
         }
    }

    if(highscore >= 0){
         ui->one_high->setPixmap(num_pixmap[h6]);
         if(highscore >= 10){
             ui->sib_high->setPixmap(num_pixmap[h5]);
             if(highscore >= 100){
                 ui->roi_high->setPixmap(num_pixmap[h4]);
                 if(highscore >= 1000){
                     ui->pan_high->setPixmap(num_pixmap[h3]);
                     if(highscore >= 10000){
                         ui->mh_high->setPixmap(num_pixmap[h2]);
                         if(highscore >= 100000){
                              ui->san_high->setPixmap(num_pixmap[h1]);
                         }
                     }
                 }
             }
         }
    }

}


void MainWindow::on_giveupp_clicked(){
    QApplication::quit();
}

void MainWindow::on_retryy_clicked(){
    for(int i = 0; i < BOARDSIZE; i++) {
        for(int j = 0; j < BOARDSIZE; j++) {
            full[j][i] = 0;
      }
    }
    cur_score = 0;
    std::cout << cur_score ;
    ui->one_cur->clear();
    ui->sib_cur->clear();
    ui->roi_cur->clear();
    ui->pan_cur->clear();
    ui->mh_cur->clear();
    ui->san_cur->clear();
    writeHighscore();
}




