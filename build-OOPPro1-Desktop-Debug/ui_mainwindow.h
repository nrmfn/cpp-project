/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.10.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QLabel *board;
    QPushButton *giveupp;
    QPushButton *retryy;
    QLabel *one_cur;
    QLabel *one_high;
    QLabel *sib_high;
    QLabel *sib_cur;
    QLabel *roi_high;
    QLabel *roi_cur;
    QLabel *pan_high;
    QLabel *pan_cur;
    QLabel *mh_high;
    QLabel *mh_cur;
    QLabel *san_high;
    QLabel *san_cur;
    QPushButton *how_to;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(438, 550);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        board = new QLabel(centralWidget);
        board->setObjectName(QStringLiteral("board"));
        board->setGeometry(QRect(20, 140, 400, 400));
        board->setMinimumSize(QSize(400, 400));
        board->setMaximumSize(QSize(400, 400));
        board->setStyleSheet(QStringLiteral("background-color: rgb(179, 179, 179);"));
        giveupp = new QPushButton(centralWidget);
        giveupp->setObjectName(QStringLiteral("giveupp"));
        giveupp->setGeometry(QRect(280, 100, 151, 31));
        giveupp->setStyleSheet(QLatin1String("\n"
"background-color:none;\n"
"\n"
"\n"
"border:none;"));
        retryy = new QPushButton(centralWidget);
        retryy->setObjectName(QStringLiteral("retryy"));
        retryy->setGeometry(QRect(280, 70, 151, 31));
        retryy->setStyleSheet(QLatin1String("\n"
"background-color:none;\n"
"\n"
"\n"
"border:none;"));
        one_cur = new QLabel(centralWidget);
        one_cur->setObjectName(QStringLiteral("one_cur"));
        one_cur->setGeometry(QRect(110, 50, 16, 16));
        one_high = new QLabel(centralWidget);
        one_high->setObjectName(QStringLiteral("one_high"));
        one_high->setGeometry(QRect(110, 100, 16, 16));
        one_high->setStyleSheet(QStringLiteral("border-color: rgb(0, 0, 0);"));
        sib_high = new QLabel(centralWidget);
        sib_high->setObjectName(QStringLiteral("sib_high"));
        sib_high->setGeometry(QRect(90, 100, 16, 16));
        sib_high->setStyleSheet(QStringLiteral("border-color: rgb(0, 0, 0);"));
        sib_cur = new QLabel(centralWidget);
        sib_cur->setObjectName(QStringLiteral("sib_cur"));
        sib_cur->setGeometry(QRect(90, 50, 16, 16));
        sib_cur->setStyleSheet(QStringLiteral("border-color: rgb(0, 0, 0);"));
        roi_high = new QLabel(centralWidget);
        roi_high->setObjectName(QStringLiteral("roi_high"));
        roi_high->setGeometry(QRect(70, 100, 16, 16));
        roi_high->setStyleSheet(QStringLiteral("border-color: rgb(0, 0, 0);"));
        roi_cur = new QLabel(centralWidget);
        roi_cur->setObjectName(QStringLiteral("roi_cur"));
        roi_cur->setGeometry(QRect(70, 50, 16, 16));
        roi_cur->setStyleSheet(QStringLiteral("border-color: rgb(0, 0, 0);"));
        pan_high = new QLabel(centralWidget);
        pan_high->setObjectName(QStringLiteral("pan_high"));
        pan_high->setGeometry(QRect(50, 100, 16, 16));
        pan_high->setStyleSheet(QStringLiteral("border-color: rgb(0, 0, 0);"));
        pan_cur = new QLabel(centralWidget);
        pan_cur->setObjectName(QStringLiteral("pan_cur"));
        pan_cur->setGeometry(QRect(50, 50, 16, 16));
        pan_cur->setStyleSheet(QStringLiteral("border-color: rgb(0, 0, 0);"));
        mh_high = new QLabel(centralWidget);
        mh_high->setObjectName(QStringLiteral("mh_high"));
        mh_high->setGeometry(QRect(30, 100, 16, 16));
        mh_high->setStyleSheet(QStringLiteral("border-color: rgb(0, 0, 0);"));
        mh_cur = new QLabel(centralWidget);
        mh_cur->setObjectName(QStringLiteral("mh_cur"));
        mh_cur->setGeometry(QRect(30, 50, 16, 16));
        mh_cur->setStyleSheet(QStringLiteral("border-color: rgb(0, 0, 0);"));
        san_high = new QLabel(centralWidget);
        san_high->setObjectName(QStringLiteral("san_high"));
        san_high->setGeometry(QRect(10, 100, 16, 16));
        san_high->setStyleSheet(QStringLiteral("border-color: rgb(0, 0, 0);"));
        san_cur = new QLabel(centralWidget);
        san_cur->setObjectName(QStringLiteral("san_cur"));
        san_cur->setGeometry(QRect(10, 50, 16, 16));
        san_cur->setStyleSheet(QStringLiteral("border-color: rgb(0, 0, 0);"));
        how_to = new QPushButton(centralWidget);
        how_to->setObjectName(QStringLiteral("how_to"));
        how_to->setGeometry(QRect(142, 0, 171, 41));
        how_to->setStyleSheet(QLatin1String("\n"
"background-color:none;\n"
"\n"
"\n"
"border:none;"));
        MainWindow->setCentralWidget(centralWidget);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", nullptr));
        board->setText(QApplication::translate("MainWindow", "TextLabel", nullptr));
        giveupp->setText(QString());
        retryy->setText(QString());
        one_cur->setText(QString());
        one_high->setText(QString());
        sib_high->setText(QString());
        sib_cur->setText(QString());
        roi_high->setText(QString());
        roi_cur->setText(QString());
        pan_high->setText(QString());
        pan_cur->setText(QString());
        mh_high->setText(QString());
        mh_cur->setText(QString());
        san_high->setText(QString());
        san_cur->setText(QString());
        how_to->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
