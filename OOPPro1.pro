#-------------------------------------------------
#
# Project created by QtCreator 2018-04-30T20:08:12
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = OOPPro1
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    lblock.cpp \
    piece.cpp \
    squareblock.cpp \
    nreblock.cpp \
    nblock.cpp \
    lreblock.cpp \
    iblock.cpp

HEADERS += \
        mainwindow.h \
    piece.h \
    lblock.h \
    squareblock.h \
    nreblock.h \
    nblock.h \
    lreblock.h \
    iblock.h

FORMS += \
        mainwindow.ui \
    dialog.ui

DISTFILES += \
    Background.jpg \
    Board.jpg \
    Board.jpg \
    bg.jpg \
    Block1.png \
    SBlock.png \
    Lblock.png \
    IBlockk.jpg \
    Iblockk.png \
    Lblockk.jpg \
    LreBlockk.jpg \
    NBlockk.jpg \
    NreBlockk.jpg \
    LreBlock.png \
    Nblockk.png \
    NreBlock.png \
    SqBlockk.jpg \
    HIgh_score \
    giveUp.png \
    retry.png \
    eight.png \
    five.png \
    four.png \
    nine.png \
    one.png \
    three.png \
    two.png \
    zero.png \
    Cur_score.png \
    Highscore.png \
    Resource/bg.jpg \
    Resource/IBlockk.jpg \
    Resource/LBlock.jpg \
    Resource/Lblockk.jpg \
    Resource/LreBlockk.jpg \
    Resource/NBlockk.jpg \
    Resource/NreBlockk.jpg \
    Resource/SqBlockk.jpg \
    Resource/Block1.png \
    Resource/Cur_score.png \
    Resource/eight.png \
    Resource/five.png \
    Resource/four.png \
    Resource/giveUp.png \
    Resource/Highscore.png \
    Resource/Iblockk.png \
    Resource/LreBlock.png \
    Resource/Nblockk.png \
    Resource/nine.png \
    Resource/NreBlock.png \
    Resource/one.png \
    Resource/retry.png \
    Resource/SBlock.png \
    Resource/seven.PNG \
    Resource/six.PNG \
    Resource/three.png \
    Resource/two.png \
    Resource/zero.PNG \
    Resource/howto1.jpg \
    Resource/howto2.jpg \
    Resource/howto3.jpg \
    Resource/howto4.jpg \
    Resource/IBlockk.jpg \
    Resource/next_but.PNG \
    Resource/under_but.PNG \
    Resource/howto_but.png
