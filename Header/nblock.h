#ifndef NBLOCK_H
#define NBLOCK_H
#include <utility>
#include <set>
#include <vector>
#include <iostream>
#include <algorithm>
#include "piece.h"

class NBlock : public Piece
{
public:
    NBlock();
    bool check(std::set<std::pair<int, int>> blocks);
    int getNum();
    QPixmap getPic();
private:
    int equal = 0;
    QPixmap pic;
};

#endif // NBLOCK_H
