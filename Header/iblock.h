#ifndef IBLOCK_H
#define IBLOCK_H
#include <utility>
#include <set>
#include <vector>
#include <iostream>
#include <algorithm>
#include "piece.h"

class IBlock : public Piece {
public:
    IBlock();
    bool check(std::set<std::pair<int, int>> blocks);
    int getNum();
    QPixmap getPic();
private:
    int equal = 0;
    QPixmap pic;
};

#endif // IBLOCK_H
