#ifndef PIECE_H
#define PIECE_H

#include <QPixmap>
#include <set>
#include <utility>

class Piece {
public:
    virtual bool check(std::set<std::pair<int, int>> blocks) = 0;
    virtual int getNum() = 0;
    virtual QPixmap getPic() = 0;
    std::set<std::pair<int, int>> normalize(std::set<std::pair<int, int>>& blocks);
};

#endif // PIECE_H
