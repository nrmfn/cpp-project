#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMouseEvent>
#include <set>
#include <utility>
#include <vector>
#include <iostream>
#include <QtGui>
#include <QLabel>
#include <QRandomGenerator>
#include <fstream>
#include <QDialog>
#include "piece.h"
#include "lblock.h"
#include "squareblock.h"
#include "nreblock.h"
#include "nblock.h"
#include "lreblock.h"
#include "iblock.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:

    void on_giveupp_clicked();

    void on_retryy_clicked();


private:
    void mouseMoveEvent(QMouseEvent *mouse_event);
    void mousePressEvent(QMouseEvent *mouse_event);
    void paintEvent(QPaintEvent *paint_event);
    void endMouseEvent();
    void nextBlock();
    void checkComplete();
    void writeHighscore();
    Piece *getRandomPiece();


    Ui::MainWindow *ui;
    QPainter painter;

    static const int BOARDSIZE = 8;
    std::set<std::pair<int, int>> drawn_blocks;
    int start_x, start_y;
    bool pressed = false;
    int full[8][8];
    QPixmap block_pixmaps[6];
    QPixmap num_pixmap[9];
    QPixmap cur_pix;
    QPixmap high_score_pix;
    QPixmap button_pix;
    QPixmap retry_pix;


    Piece *current, *next;

    int highscore;
    int cur_score = 0;


};

#endif // MAINWINDOW_H
