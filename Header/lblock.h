#ifndef LBLOCK_H
#define LBLOCK_H
#include <utility>
#include <set>
#include <vector>
#include <iostream>
#include <algorithm>
#include "piece.h"

class LBlock : public Piece {
public:
    LBlock();
    bool check(std::set<std::pair<int, int>> blocks);
    int getNum();
    QPixmap getPic();
private:
    int equal = 0;
    QPixmap pic;
};

#endif // LBLOCK_H
