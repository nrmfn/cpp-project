#ifndef SQUAREBLOCK_H
#define SQUAREBLOCK_H
#include <utility>
#include <set>
#include <iostream>
#include <algorithm>
#include "piece.h"

class SquareBlock : public Piece {
public:
    SquareBlock();
    bool check(std::set<std::pair<int, int>> blocks);
    int getNum();
    QPixmap getPic();
private:
    int equal = 0;
    QPixmap pic;
};

#endif // SQUAREBLOCK_H
