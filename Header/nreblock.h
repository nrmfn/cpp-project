#ifndef NREBLOCK_H
#define NREBLOCK_H
#include <utility>
#include <set>
#include <vector>
#include <iostream>
#include <algorithm>
#include "piece.h"

class NreBlock : public Piece{
public:
    NreBlock();
    bool check(std::set<std::pair<int, int>> blocks);
    int getNum();
    QPixmap getPic();
private:
    int equal = 0;
    QPixmap pic;
};

#endif // NREBLOCK_H
