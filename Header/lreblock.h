#ifndef LREBLOCK_H
#define LREBLOCK_H
#include <utility>
#include <set>
#include <vector>
#include <iostream>
#include <algorithm>
#include "piece.h"

class LreBlock : public Piece{
public:
    LreBlock();
    bool check(std::set<std::pair<int, int>> blocks);
    int getNum();
    QPixmap getPic();
private:
    int equal = 0;
    QPixmap pic;
};

#endif // LREBLOCK_H
