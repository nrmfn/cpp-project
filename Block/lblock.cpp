#include "lblock.h"

LBlock::LBlock() {
    //Pic to display
    pic.load("/Users/pasutapaopun/Desktop/OOPPro1/Resource/lBlockk.jpg");
}

//Check if the drawn block is correct
bool LBlock::check(std::set<std::pair<int, int> > blocks) {
    blocks = normalize(blocks);
    std::set<std::pair<int, int>>::iterator it = blocks.begin();

    //Rotate coordinates
    std::set<std::pair<int, int>> check1 = {{0,0},{0,1},{0,2},{1,2}};
    std::set<std::pair<int, int>> check2 = {{0,0},{0,1},{1,0},{2,0}};
    std::set<std::pair<int, int>> check3 = {{0,0},{1,0},{1,1},{1,2}};
    std::set<std::pair<int, int>> check4 = {{2,0},{2,1},{1,1},{0,1}};
    std::vector<std::pair<int, int>> temp_set= {};

    std::set_intersection
            (blocks.begin(), blocks.end(), check1.begin(), check1.end(), std::back_inserter(temp_set));
    if (temp_set.size() == 4) {
        return true;
    }
    temp_set.clear();

    std::set_intersection
            (blocks.begin(), blocks.end(), check2.begin(), check2.end(), std::back_inserter(temp_set));
    if (temp_set.size() == 4) {
        return true;
    }
    temp_set.clear();

    std::set_intersection
            (blocks.begin(), blocks.end(), check3.begin(), check3.end(), std::back_inserter(temp_set));
    if (temp_set.size() == 4) {
        return true;
    }
    temp_set.clear();

    std::set_intersection
            (blocks.begin(), blocks.end(), check4.begin(), check4.end(), std::back_inserter(temp_set));
    if (temp_set.size() == 4) {
        return true;
    }
    temp_set.clear();


}

int LBlock::getNum() {
    return 1;
}

QPixmap LBlock::getPic() {
    return pic;
}


