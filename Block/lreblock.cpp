#include "lreblock.h"

LreBlock::LreBlock(){
    //Pic to display
    pic.load("/Users/pasutapaopun/Desktop/OOPPro1/Resource/LreBlockk.jpg");
}

//Check if the drawn block is correct
bool LreBlock::check(std::set<std::pair<int, int> > blocks){
    blocks = normalize(blocks);
    std::set<std::pair<int, int>>::iterator it = blocks.begin();

    //Rotate coordinates
    std::set<std::pair<int, int>> check1 = {{0,0},{0,1},{0,2},{1,0}};
    std::set<std::pair<int, int>> check2 = {{0,0},{0,1},{1,1},{2,1}};
    std::set<std::pair<int, int>> check3 = {{1,0},{1,1},{1,2},{0,2}};
    std::set<std::pair<int, int>> check4 = {{0,0},{1,0},{2,0},{2,1}};

    std::vector<std::pair<int, int>> temp_set= {};

    std::set_intersection
            (blocks.begin(), blocks.end(), check1.begin(), check1.end(), std::back_inserter(temp_set));
    if (temp_set.size() == 4) {
        return true;
    }
    temp_set.clear();

    std::set_intersection
            (blocks.begin(), blocks.end(), check2.begin(), check2.end(), std::back_inserter(temp_set));
    if (temp_set.size() == 4) {
        return true;
    }
    temp_set.clear();

    std::set_intersection
            (blocks.begin(), blocks.end(), check3.begin(), check3.end(), std::back_inserter(temp_set));
    if (temp_set.size() == 4) {
        return true;
    }
    temp_set.clear();

    std::set_intersection
            (blocks.begin(), blocks.end(), check4.begin(), check4.end(), std::back_inserter(temp_set));
    if (temp_set.size() == 4) {
        return true;
    }
    temp_set.clear();

}

int LreBlock::getNum(){
    return 5;
}

QPixmap LreBlock::getPic(){
    return pic;
}
