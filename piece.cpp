#include "piece.h"

std::set<std::pair<int, int>> Piece::normalize(std::set<std::pair<int, int>> &blocks) {
    std::set<std::pair<int, int>>::iterator it = blocks.begin();
    int min_x = it->first, min_y = it->second;
    for  (; it != blocks.end(); it++) {
        int x = it->first, y = it->second;
        if (x < min_x) {
            min_x = x;
        }
        if (y < min_y) {
            min_y = y;
        }
    }
    std::set<std::pair<int, int>> normalized_set;
    it = blocks.begin();
    for  (; it != blocks.end(); it++) {
        normalized_set.insert(std::pair<int, int>((*it).first - min_x, (*it).second - min_y));
    }
    return normalized_set;
}
